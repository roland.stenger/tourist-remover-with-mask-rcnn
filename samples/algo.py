import os, sys, glob
import random
import math
import numpy as np
import skimage.io
from scipy.signal import convolve2d
import matplotlib.pyplot as plt
import matplotlib.image as mplim

from model import loadPretrainedModel, class_names

#%%
        
        
"""
PIPELINE:
    1. load images from predefined folder with 
        images = getImagesBySubName(subname),
        (all images with the pattern 'subname' in the name will 
        be loaded into the stack of images)
        
        1.1 load algorithm and set images with 
            masker = PersonRemover(images)
            
    2. OPTIONAL: Use Mask-RCNN to mask instances pixelwise in each image with a marker-color,
        when it comes to the pixelwise median, the pixel with the special color will be ignored
        using numpy.nanmedian
        
    3. Use class-function simpleMedian() for the default median, (removes every moving object),
        or mask people with maskPeopleInImages and set the new masked images in the algorithm to 
        be processed.
    
    4. Save images with saveImage(name, image)

"""

#%%

def getImagesFromFolder(folder='raw_images/samui/'):
    images = glob.glob(folder + '*')
    data = []
    for image in images:
        image = skimage.io.imread(image)
        data.append(image[:,:,:3]) 
        
    # shape is [num_images, height, width, 3 (rgb)]
    return np.asarray(data)
        
def medianFromImages(images):
    res = np.median(images, axis=0)
    return res

def loadModel():
    return loadPretrainedModel()

def maskObjectsInImages(images, model=None, color=(255,0,0), plot=False, objects=[1,2,3,4,6,8]):
    if model==None:
        model=loadModel()
        
    images_masked = []
    for i, image in enumerate(images):
        print('Mask image {}'.format(i))
        r = model.detect([image], verbose=0)[0]
                
        image_masked = image.copy()
        
        for mask, class_id in zip(r['masks'].T, r['class_ids']):
            # masks person, bike, car, motorcycle, bus, truck
            if class_id in objects:
                mask = mask.T
                mask_inv = np.invert(mask)
                        
                image_masked[:,:,0] = image_masked[:,:,0]*mask_inv + (mask*color[0])
                image_masked[:,:,1] = image_masked[:,:,1]*mask_inv + (mask*color[1])
                image_masked[:,:,2] = image_masked[:,:,2]*mask_inv + (mask*color[2])
        images_masked.append(image_masked[:,:,:3])
    images_masked = np.asarray(images_masked)
        
    return images_masked


def maskPeopleInImages(images, model=None, color=(255,0,0), plot=False):
    if model==None:
        model=loadModel()
        
    images_masked = []
    for i, image in enumerate(images):
        print('Mask image {}'.format(i))
        r = model.detect([image], verbose=0)[0]
                
        image_masked = image.copy()
        
        for mask, class_id in zip(r['masks'].T, r['class_ids']):
            # masks person, bike, car, motorcycle, bus, truck
            if class_id in [1,2,3,4,6,8]:
                mask = mask.T
                mask_inv = np.invert(mask)
                        
                image_masked[:,:,0] = image_masked[:,:,0]*mask_inv + (mask*color[0])
                image_masked[:,:,1] = image_masked[:,:,1]*mask_inv + (mask*color[1])
                image_masked[:,:,2] = image_masked[:,:,2]*mask_inv + (mask*color[2])
        images_masked.append(image_masked[:,:,:3])
    images_masked = np.asarray(images_masked)
        
    return images_masked

def medianWithColorIgnore(images, ref_image, color=(255,0,0)):
    nanimages = []
    
    for i, image in enumerate(images):
        inds = np.asarray(np.where(np.all(image==color, axis=-1))).T
            
        image = np.array(image, dtype=np.float)
        image[inds[:,0], inds[:,1],:] = np.nan
            
        nanimages.append(image)
    nanimages = np.asarray(nanimages)
    res = np.nanmedian(nanimages, axis=0)
        
    res = np.nan_to_num(res)
    nan_inds = np.asarray(np.where(np.all(res==[0,0,0], axis=-1))).T
    
    #print(nan_inds)
    res[nan_inds[:,0], nan_inds[:,1],:] = ref_image[nan_inds[:,0], nan_inds[:,1],:]
        
    return res

def saveImage(name, image):
        mplim.imsave(name, image)
#%%

def getImagesBySubName(subname='a', folder='oberpropics/'):
    images = glob.glob(folder + '*' + subname + '*')
    data = []
    for image in images:
        image = skimage.io.imread(image)
        data.append(image[:,:,:3]) 
        
    # shape is [num_images, height, width, 3 (rgb)]
    return np.asarray(data)


    
def simpleMedian(subname, plot=True, save=True, savename='_simple.png'):   
    images = getImagesBySubName(subname)
    remover = PersonRemover(images, person_detection=True)
        
    result = remover.simpleMedian()
    if plot:
        remover.plotImage(result)
    remover.saveImage(subname + savename, result/255)   
    
    return result
    
#%%
    
class PersonRemover:
    def __init__(self, images=None, person_detection=False):
        """

        Parameters
        ----------
        images : float-array [stack, height, width], optional
            DESCRIPTION. The default is None.
        person_detection : bool, Mask-RCNN should be activated? If not, 
            medianWithColorIgnore and maskPeopleImages is not available

        Returns
        -------
        None.

        """
        self.images = images
        if person_detection:
            self.model = loadPretrainedModel()
        else:
            self.model = None
            
    def setReferenceImage(self, image):
        self.reference = image
    
    def addImage(self, image):
        self.images = np.append(self.image, image)
        
    def simpleMedian(self):
        res = np.median(self.images.T, axis=3).T
        return res
    
    
    def medianWithColorIgnore(self, color=(255,0,0)):
        """

        Parameters
        ----------
        color : three int, this color is ignored when it comes to the pixelwise median
            of the stack of images in self.images, when nothing is marked, it is the normal median
            DESCRIPTION. The default is (255,0,0).

        Returns
        -------
        res : image
            the result of the median, hopefully without tourists

        """
        nanimages = []
        for i, image in enumerate(self.images):
            inds = np.asarray(np.where(np.all(image==color, axis=-1))).T
            
            image = np.array(image, dtype=np.float)
            image[inds[:,0], inds[:,1],:] = np.nan
            
            nanimages.append(image)
        nanimages = np.asarray(nanimages)
        res = np.nanmedian(nanimages.T, axis=3).T
        
        res = np.nan_to_num(res)
        nan_inds = np.asarray(np.where(np.all(res==[0,0,0], axis=-1))).T
        print(nan_inds)
        res[nan_inds[:,0], nan_inds[:,1],:] = self.reference[nan_inds[:,0], nan_inds[:,1],:]

        
        return res
    
    def maskPeopleInImages(self, color=(255,0,0), plot=False):
        """
        Parameters
        ----------
        color : three int, the color in which the detected objects are masked
            The default is (255,0,0).
        plot : bool, plot results?
        Returns
        -------
        images_masked : image-array with rgb-channel, detected objects are masked in color

        """
        if self.model is not None:
            images_masked = []
            for i, image in enumerate(self.images):
                print('Mask image {}'.format(i))
                r = self.model.detect([image], verbose=0)[0]
                
                image_masked = image.copy()
                
                for mask, class_id in zip(r['masks'].T, r['class_ids']):
                    # masks person, bike, car, motorcycle, bus, truck
                    if class_id in [1,2,3,4,6,8]:
                        mask = mask.T
                        mask_inv = np.invert(mask)
                        
                        image_masked[:,:,0] = image_masked[:,:,0]*mask_inv + (mask*color[0])
                        image_masked[:,:,1] = image_masked[:,:,1]*mask_inv + (mask*color[1])
                        image_masked[:,:,2] = image_masked[:,:,2]*mask_inv + (mask*color[2])
                        
                if plot:
                    self.plotImage(image_masked)
                images_masked.append(image_masked[:,:,:3])
            images_masked = np.asarray(images_masked)
            
        return images_masked
    
    
    def maskObjectsInImages(self, objects=['person'], color=(255,0,0), plot=False):

        object_code = {'person':1, 'bicycle':2, 'car':3, 
                       'motorcycle':4, 'airplane':5,'bus':6, 
                       'train':7, 'truck':8, 'boat':9,
                       'bird':15,'cat':16, 'dog':17, 'horse':18, 
                       'sheep':19, 'cow':20, 'elephant':21, 
                       'bear':22,'zebra':23, 'giraffe':24}
        class_ids = [object_code[obj] for obj in objects]
        
        if self.model is not None:
            images_masked = []
            for i, image in enumerate(self.images):
                print('Mask image {}'.format(i))
                r = self.model.detect([image], verbose=0)[0]
                
                image_masked = image.copy()
                
                for mask, class_id in zip(r['masks'].T, r['class_ids']):
                    # masks person, bike, car, motorcycle, bus, truck
                    
                    if class_id in class_ids:
                        mask = mask.T
                        mask_inv = np.invert(mask)
                        
                        image_masked[:,:,0] = image_masked[:,:,0]*mask_inv + (mask*color[0])
                        image_masked[:,:,1] = image_masked[:,:,1]*mask_inv + (mask*color[1])
                        image_masked[:,:,2] = image_masked[:,:,2]*mask_inv + (mask*color[2])
                        
                if plot:
                    self.plotImage(image_masked)
                images_masked.append(image_masked[:,:,:3])
            images_masked = np.asarray(images_masked)
            
        return images_masked
    
    def plotImage(self, image):
        fig, ax = plt.subplots(figsize=(16, 8))
        ax.imshow(image.astype(np.uint8))
        plt.show()
    
    def saveImage(self, name, image):
        mplim.imsave(name, image)
              
        
        
        