import matplotlib.pyplot as plt
import numpy as np
from algo import getImagesFromFolder, medianFromImages, loadModel, maskPeopleInImages, medianWithColorIgnore, saveImage
from algo import maskObjectsInImages

from mrcnn import visualize

class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
               'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']

#%%
name_id = 'scate'

#%%
def visualizeMasks(image, model, processed_folder, save=True, savename='masks.jpg'):
    results = model.detect([image], verbose=0)
    
    # Visualize results
    r = results[0]
    res = visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'], 
                                class_names, r['scores'])
    
    if save:
        saveImage(processed_folder + savename, res/255)
        
    return res

#%%
def median(images, processed_folder, savename='result.png'):        
    res = medianFromImages(images)

    saveImage(processed_folder + savename, res/255)

#%%
def maskrcnn_median(images, processed_folder, model=None, savename='mask_result.png'):
    if model==None:
        print('Load model')
        model = loadModel()
        
    ref_image = images[0]

    print('Mask People with color...')
    images_masked = maskPeopleInImages(images, model=model)
    
    for i, image in enumerate(images_masked):
        saveImage(processed_folder + f'masked_{i}.png', image)
    print('do median with ignoring color')
    res = medianWithColorIgnore(images_masked, ref_image)
    
    saveImage(processed_folder + savename, res/255)
    return res, images_masked

#%%
    
raw_folder = f'raw_images/{name_id}/'
images = getImagesFromFolder(folder=raw_folder)

processed_folder = f'processed_images/{name_id}/'
savename = 'median.png'

mode = 3 # choose value, depends on what you want

try:
    model
except NameError:
    model = loadModel()
    
if mode==0:
    # onyl median
    median(images, processed_folder, savename)    

elif mode==1:
    # visualize masks in image
    visualizeMasks(images[1], model,processed_folder, savename='masks_1.png')
    
elif mode==2:
    # do mask rcnn
    res = maskrcnn_median(images, processed_folder, model=model)
    
elif mode==3:
    # do median and mask
    median(images, processed_folder, savename)
    res = maskrcnn_median(images, processed_folder, model=model)
    
    
    
    
    
    
    
    
    
