Algorithmus basiert auf dem Mask-RCNN-Model aus 
https://github.com/matterport/Mask_RCNN, siehe README_MaskRCNN

Start:
samples/main.py oder jupyter-notebook-Datei ausführen, abhängig vom angegebenen Namen (name_id='street' default) 
nimmt er Bilder aus dem samples/raw_images-Ordner und speichert die Ergebnisse in samples/processed_images